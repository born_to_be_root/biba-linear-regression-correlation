# Assignment 1 for BI-BA subject.

# Group members

* MSE0084 - DO Tien Dung
* MSE0087 - DO Viet Quan
* MSE0088 - NGUYEN Ngoc Tram
* MSE0099 - NGUYEN Dinh Phuong

# Requirements

* Data `admission.csv`
* Library `e1071`

# Linear Regression Model

Based on training dataset (split from original dataset) to predict GPA based on GMAT score.